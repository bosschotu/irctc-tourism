package com.spicemoney.tourism.provider.irctc.service;

import com.spicemoney.tourism.dto.responseDto.PackageSearchDto;
import com.spicemoney.tourism.encryption.EncryptionService;
import com.spicemoney.tourism.provider.Provider;
import com.spicemoney.tourism.provider.irctc.requestDto.PackageListRequestDto;
import com.spicemoney.tourism.provider.irctc.responseDto.PackageItineraryResponseDto;
import com.spicemoney.tourism.provider.irctc.responseDto.PackageListResponseDto;
import com.spicemoney.tourism.provider.irctc.responseDto.PackageTermsDetailsResponseDto;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;


public class IrctcTourism implements Provider {

    private final String baseUrl;
    private final EncryptionService irctcEncryptionService;
    private final RestTemplate irctcRestTemplate;

    public IrctcTourism(String baseUrl, EncryptionService irctcEncryptionService, RestTemplate irctcRestTemplate) {
        this.baseUrl = baseUrl;
        this.irctcEncryptionService = irctcEncryptionService;
        this.irctcRestTemplate = irctcRestTemplate;
    }

    @Override
    public List<PackageSearchDto> getPackageSearchkey(String searchKey, String travelType) {
        final String peckageSearchUri = UriComponentsBuilder.fromUriString(baseUrl + "/tourismpApi/pckgsApi/search")
                .queryParam("searchKey", searchKey)
                .queryParam("travelType", travelType)
                .queryParam("sector", "All")
                .toUriString();
        // set headers
        HttpHeaders loginHeaders = new HttpHeaders();
        loginHeaders.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> loginEntity = new HttpEntity<String>(loginHeaders);

        ResponseEntity<String> response = irctcRestTemplate.exchange(peckageSearchUri, HttpMethod.POST, loginEntity, String.class);
        JSONObject responseBody = new JSONObject(response.getBody());
        if (response.getStatusCode() == HttpStatus.OK && "SUCCESS".equalsIgnoreCase(responseBody.getString("status"))) {
            return responseBody.getJSONArray("data")
                    .toList().stream()
                    .map(d -> (JSONObject) d)
//                    .filter(data -> searchType.map(s -> s.equalsIgnoreCase(data.getString("TAG_TYPE"))).orElse(true))
                    .map(data -> new PackageSearchDto(data.getString("SEARCH_KEY"), data.getString("TAG_TYPE")))
                    .collect(Collectors.toList());
        }
        return Collections.emptyList();
    }

    @Override
    public List<PackageListResponseDto> searchPackage(String source, String destination, String travelType) {

        final String packageUri = baseUrl + "/tourismpApi/pckgsApi/searchPkgList";
        PackageListRequestDto packageListDto = new PackageListRequestDto(source, "origin", travelType);
        String encryptData = irctcEncryptionService.encrypt(new JSONObject(packageListDto).toString());
        String srchrqst = new JSONObject().put("srchrqst", encryptData).toString();
        String token = login();
        HttpHeaders requestHeaders = httpHeadersWithAuthorization(token);
        HttpEntity<String> requestEntity = new HttpEntity<String>(srchrqst, requestHeaders);
        ResponseEntity<String> response = irctcRestTemplate.exchange(packageUri, HttpMethod.POST, requestEntity, String.class);
        JSONObject responseBody = new JSONObject(response.getBody());
        if (response.getStatusCode() == HttpStatus.OK && "SUCCESS".equalsIgnoreCase(responseBody.getString("status"))) {
            JSONArray responseData = new JSONArray(irctcEncryptionService.decrypt(responseBody.getString("data")));
            System.out.println("==================================================="+responseData);
            responseData.toList().stream().forEach(data -> System.out.println("========================="+data));
            return responseData.toList()
                            .stream()
                            .map(d-> (JSONObject) d)
                            .map( data -> setPackageList(data))
                            .collect(Collectors.toList());
        }
        return Collections.emptyList();
    }

    @Override
    public PackageItineraryResponseDto getPackageItinerary(String pckgCode){
        final String url = UriComponentsBuilder.fromUriString(baseUrl + "/tourismpApi/pckgsApi/getItineraryDetails")
                .queryParam("pckgCode", pckgCode)
                .toUriString();
        HttpHeaders header = new HttpHeaders();
        header.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> entity = new HttpEntity<String>(header);
        ResponseEntity<String> response = irctcRestTemplate.exchange(url, HttpMethod.POST, entity, String.class);
        JSONObject responseBody = new JSONObject(response.getBody());
        if(response.getStatusCode() == HttpStatus.OK && "SUCCESS".equalsIgnoreCase(responseBody.getString("status"))){
            return new PackageItineraryResponseDto(
                    responseBody.getJSONObject("data").getString("pckgCode"),
                    responseBody.getJSONObject("data").getString("pckgName"),
                    responseBody.getJSONObject("data").getString("impNotes")
            );
        }
        return new PackageItineraryResponseDto();
    }

    @Override
    public PackageTermsDetailsResponseDto getPackageTermsDetails(String pckgCode){
        final String url = UriComponentsBuilder.fromUriString(baseUrl + "/tourismpApi/pckgsApi/getPackageDetailsTerms")
                .queryParam("pckgCode", pckgCode)
                .toUriString();
        HttpHeaders header = new HttpHeaders();
        header.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> entity = new HttpEntity<String>(header);
        ResponseEntity<String> response = irctcRestTemplate.exchange(url, HttpMethod.POST, entity, String.class);
        JSONObject responseBody = new JSONObject(response.getBody());
        if(response.getStatusCode() == HttpStatus.OK && "SUCCESS".equalsIgnoreCase(responseBody.getString("status"))){
            return  new PackageTermsDetailsResponseDto(
                        responseBody.getJSONObject("data").getString("description"),
                        responseBody.getJSONObject("data").getString("pckgCode"),
                        responseBody.getJSONObject("data").getJSONArray("impNotesLst")
                                .toList().stream().map(d -> (String) d).collect(Collectors.toList())
                    );
        }
        return new PackageTermsDetailsResponseDto();
    }

//    @Override
//    public void searchPackage(String source, String travelType) {
//        searchPackage(source, null, travelType);
//    }

    private String login() {
        final String loginUri = baseUrl + "/userlogin/user/login";
        // create request body
        JSONObject loginRequest = new JSONObject();
        loginRequest.put("username", "spice_test");
        loginRequest.put("password", "spice_test");
        // set headers
        HttpHeaders loginHeaders = new HttpHeaders();
        loginHeaders.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> loginEntity = new HttpEntity<String>(loginRequest.toString(), loginHeaders);
        // send request and parse result
        ResponseEntity<String> loginTokenResponse = irctcRestTemplate.exchange(loginUri, HttpMethod.POST, loginEntity, String.class);
        JSONObject responseBody = new JSONObject(loginTokenResponse.getBody());
        if (loginTokenResponse.getStatusCode() == HttpStatus.OK && "SUCCESS".equalsIgnoreCase(responseBody.getString("status"))) {
            return responseBody.getString("data");
        } else if (loginTokenResponse.getStatusCode() == HttpStatus.UNAUTHORIZED) {
            return "";
        }
        return "";
    }

    private HttpHeaders httpHeadersWithAuthorization(String token) {
        HttpHeaders requestHeaders = new HttpHeaders();
        requestHeaders.setContentType(MediaType.APPLICATION_JSON);
        requestHeaders.add("Authorization", "Bearer " + token);
        return requestHeaders;
    }

    private PackageListResponseDto setPackageList(JSONObject obj){
        PackageListResponseDto dto = new PackageListResponseDto();
        dto.setIS_CRUISE(obj.getString("IS_CRUISE"));
        dto.setDEPARTURE(obj.getString("DEPARTURE"));
        dto.setSECTOR(obj.getString("SECTOR"));
        dto.setPKG_NAME(obj.getString("PKG_NAME"));
        dto.setFARE(Long.parseLong(obj.getString("FARE")));
        dto.setDURATION(obj.getString("DURATION"));
        dto.setDESTINATION(obj.getString("DEST"));
        dto.setPKG_CODE(obj.getString("PKG_CODE"));
        dto.setORIGIN(obj.getString("ORIGIN"));
        dto.setTRAVEL_TYPE(obj.getString("TRAVEL_TYPE"));
        dto.setIMG(obj.getString("IMG"));
        dto.setDATE_OF_JOURNEY(obj.getString("DATE_OF_JOURNEY"));
        return dto;
    }
}


