package com.spicemoney.tourism.provider.irctc.responseDto;

import java.util.ArrayList;
import java.util.List;

public class PackageTermsDetailsResponseDto {
    private String description;
    private String pckgCode;
    private List<String> impNotes;

    public PackageTermsDetailsResponseDto(){};

    public PackageTermsDetailsResponseDto(String description, String pckgCode, List<String> impNotes){
        this.description = description;
        this.pckgCode = pckgCode;
        this.impNotes = impNotes;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPckgCode() {
        return pckgCode;
    }

    public void setPckgCode(String pckgCode) {
        this.pckgCode = pckgCode;
    }

    public List<String> getImpNotes() {
        return impNotes;
    }

    public void setImpNotes(List<String> impNotes) {
        this.impNotes = impNotes;
    }
}
