package com.spicemoney.tourism.provider.irctc.requestDto;

public class PackageListRequestDto {
    private String searchKey;
    private String tagType;
    private String sector="All";
    private String tourType;
    private int bDar = 0;
    private int acCharter = 0;
    private String isCruise = "";
    private String isHeritage = "";
    private String src = "test";

    public PackageListRequestDto() {
    }

    public PackageListRequestDto(String searchKey, String tagType, String tourType){
        this.searchKey = searchKey;
        this.tagType = tagType;
        this.tourType = tourType;
    }

    public PackageListRequestDto(
            String searchKey, String tagType, String sector, String tourType,
            int bDar, int acCharter, String isCruise, String isHeritage, String src
    ) {
        this.searchKey = searchKey;
        this.tagType = tagType;
        this.sector = sector;
        this.tourType = tourType;
        this.bDar = bDar;
        this.acCharter = acCharter;
        this.isCruise = isCruise;
        this.isHeritage = isHeritage;
        this.src = src;
    }

    public String getSearchKey() {
        return searchKey;
    }

    public void setSearchKey(String searchKey) {
        this.searchKey = searchKey;
    }

    public String getTagType() {
        return tagType;
    }

    public void setTagType(String tagType) {
        this.tagType = tagType;
    }

    public String getSector() {
        return sector;
    }

    public void setSector(String sector) {
        this.sector = sector;
    }

    public String getTourType() {
        return tourType;
    }

    public void setTourType(String tourType) {
        this.tourType = tourType;
    }

    public int getbDar() {
        return bDar;
    }

    public void setbDar(int bDar) {
        this.bDar = bDar;
    }

    public int getAcCharter() {
        return acCharter;
    }

    public void setAcCharter(int acCharter) {
        this.acCharter = acCharter;
    }

    public String getIsCruise() {
        return isCruise;
    }

    public void setIsCruise(String isCruise) {
        this.isCruise = isCruise;
    }

    public String getIsHeritage() {
        return isHeritage;
    }

    public void setIsHeritage(String isHeritage) {
        this.isHeritage = isHeritage;
    }

    public String getSrc() {
        return src;
    }

    public void setSrc(String src) {
        this.src = src;
    }
}
