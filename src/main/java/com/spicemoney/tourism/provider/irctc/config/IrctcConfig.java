package com.spicemoney.tourism.provider.irctc.config;

import com.spicemoney.tourism.encryption.EncryptionService;
import com.spicemoney.tourism.provider.Provider;
import com.spicemoney.tourism.provider.irctc.service.IrctcTourism;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;

@Configuration
public class IrctcConfig {

    @Value("${irctc.base.url}")
    public String baseUrl;

    @Resource
    private EncryptionService irctcEncryptionService;

    @Bean
    public RestTemplate irctcRestTemplate(){
        return new RestTemplate();
    }

    @Bean
    public Provider irctcTourism(){
        return new IrctcTourism(baseUrl, irctcEncryptionService, irctcRestTemplate());
    }
}
