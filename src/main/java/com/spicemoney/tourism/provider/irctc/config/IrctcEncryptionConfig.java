package com.spicemoney.tourism.provider.irctc.config;

import com.spicemoney.tourism.encryption.EncryptionService;
import com.spicemoney.tourism.provider.irctc.encryption.IrctcEncryptionService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.security.spec.AlgorithmParameterSpec;

@Configuration
public class IrctcEncryptionConfig {

    @Value("${irctc.aes.encrypt.key}")
    public String key;

    @Value("${irctc.aes.encrypt.spec}")
    public String ivSpec;

    @Value("${irctc.encrypt.algorithm:AES}")
    public String algorithm;

    @Bean
    public AlgorithmParameterSpec algorithmParameterSpec() {
        return new IvParameterSpec(ivSpec.getBytes());
    }

    @Bean
    public SecretKey secretKey() {
        return new SecretKeySpec(key.getBytes(), "AES");
    }

    @Bean
    public EncryptionService irctcEncryptionService() {
        return new IrctcEncryptionService(algorithm, secretKey(), algorithmParameterSpec());
    }
}
