package com.spicemoney.tourism.provider.irctc.responseDto;

public class PackageListResponseDto {
    private String WB;
    private String IDT;
    private String IBT;
    private String IAT;
    private String ITT;
    private String IRT;
    private String IHA;
    private String IM;
    private String IGE;
    private String ITI;
    private String OPTFLI;
    private String IS_CRUISE;
    private String DEPARTURE;
    private String IS_UTS;
    private String SECTOR;
    private String PKG_NAME;
    private long FARE;
    private String DURATION;
    private String PDM;
    private String DESTINATION;
    private String PKG_CODE;
    private String ORIGIN;
    private String TRAVEL_TYPE;
    private String IMG;
    private String DATE_OF_JOURNEY;

    public String getWB() {
        return WB;
    }

    public void setWB(String WB) {
        this.WB = WB;
    }

    public String getIDT() {
        return IDT;
    }

    public void setIDT(String IDT) {
        this.IDT = IDT;
    }

    public String getIBT() {
        return IBT;
    }

    public void setIBT(String IBT) {
        this.IBT = IBT;
    }

    public String getIAT() {
        return IAT;
    }

    public void setIAT(String IAT) {
        this.IAT = IAT;
    }

    public String getITT() {
        return ITT;
    }

    public void setITT(String ITT) {
        this.ITT = ITT;
    }

    public String getIRT() {
        return IRT;
    }

    public void setIRT(String IRT) {
        this.IRT = IRT;
    }

    public String getIHA() {
        return IHA;
    }

    public void setIHA(String IHA) {
        this.IHA = IHA;
    }

    public String getIM() {
        return IM;
    }

    public void setIM(String IM) {
        this.IM = IM;
    }

    public String getIGE() {
        return IGE;
    }

    public void setIGE(String IGE) {
        this.IGE = IGE;
    }

    public String getITI() {
        return ITI;
    }

    public void setITI(String ITI) {
        this.ITI = ITI;
    }

    public String getOPTFLI() {
        return OPTFLI;
    }

    public void setOPTFLI(String OPTFLI) {
        this.OPTFLI = OPTFLI;
    }

    public String getIS_CRUISE() {
        return IS_CRUISE;
    }

    public void setIS_CRUISE(String IS_CRUISE) {
        this.IS_CRUISE = IS_CRUISE;
    }

    public String getDEPARTURE() {
        return DEPARTURE;
    }

    public void setDEPARTURE(String DEPARTURE) {
        this.DEPARTURE = DEPARTURE;
    }

    public String getIS_UTS() {
        return IS_UTS;
    }

    public void setIS_UTS(String IS_UTS) {
        this.IS_UTS = IS_UTS;
    }

    public String getSECTOR() {
        return SECTOR;
    }

    public void setSECTOR(String SECTOR) {
        this.SECTOR = SECTOR;
    }

    public String getPKG_NAME() {
        return PKG_NAME;
    }

    public void setPKG_NAME(String PKG_NAME) {
        this.PKG_NAME = PKG_NAME;
    }

    public long getFARE() {
        return FARE;
    }

    public void setFARE(long FARE) {
        this.FARE = FARE;
    }

    public String getDURATION() {
        return DURATION;
    }

    public void setDURATION(String DURATION) {
        this.DURATION = DURATION;
    }

    public String getPDM() {
        return PDM;
    }

    public void setPDM(String PDM) {
        this.PDM = PDM;
    }

    public String getDESTINATION() {
        return DESTINATION;
    }

    public void setDESTINATION(String DESTINATION) {
        this.DESTINATION = DESTINATION;
    }

    public String getPKG_CODE() {
        return PKG_CODE;
    }

    public void setPKG_CODE(String PKG_CODE) {
        this.PKG_CODE = PKG_CODE;
    }

    public String getORIGIN() {
        return ORIGIN;
    }

    public void setORIGIN(String ORIGIN) {
        this.ORIGIN = ORIGIN;
    }

    public String getTRAVEL_TYPE() {
        return TRAVEL_TYPE;
    }

    public void setTRAVEL_TYPE(String TRAVEL_TYPE) {
        this.TRAVEL_TYPE = TRAVEL_TYPE;
    }

    public String getIMG() {
        return IMG;
    }

    public void setIMG(String IMG) {
        this.IMG = IMG;
    }

    public String getDATE_OF_JOURNEY() {
        return DATE_OF_JOURNEY;
    }

    public void setDATE_OF_JOURNEY(String DATE_OF_JOURNEY) {
        this.DATE_OF_JOURNEY = DATE_OF_JOURNEY;
    }
}
