package com.spicemoney.tourism.provider.irctc.encryption;

import com.spicemoney.tourism.encryption.EncryptionException;
import com.spicemoney.tourism.encryption.EncryptionService;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import java.security.spec.AlgorithmParameterSpec;
import java.util.Base64;

public class IrctcEncryptionService implements EncryptionService {

    private final String algorithm;
    private final SecretKey key;
    private final AlgorithmParameterSpec spec;

    public IrctcEncryptionService(String algorithm, SecretKey key, AlgorithmParameterSpec spec) {
        this.algorithm = algorithm;
        this.key = key;
        this.spec = spec;
    }

    public String encrypt(String input) throws EncryptionException {
        try {
            Cipher cipher = Cipher.getInstance(algorithm);
            cipher.init(Cipher.ENCRYPT_MODE, key, spec);
            byte[] cipherText = cipher.doFinal(input.getBytes());
            return Base64.getEncoder().encodeToString(cipherText);
        } catch (Exception e) {
            throw new EncryptionException(e);
        }
    }

    public String decrypt(String cipherText) throws EncryptionException {
        try {
            Cipher cipher = Cipher.getInstance(algorithm);
            cipher.init(Cipher.DECRYPT_MODE, key, spec);
            byte[] plainText = cipher.doFinal(Base64.getDecoder().decode(cipherText));
            return new String(plainText);
        } catch (Exception e) {
            throw new EncryptionException(e);
        }
    }
}
