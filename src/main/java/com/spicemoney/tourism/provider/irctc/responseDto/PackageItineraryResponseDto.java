package com.spicemoney.tourism.provider.irctc.responseDto;

public class PackageItineraryResponseDto {
    private String pckgCode;
    private String pckgName;
    private String impNotes;

    public PackageItineraryResponseDto(){};

    public PackageItineraryResponseDto(String pckgCode, String pckgName, String impNotes){
        this.pckgCode = pckgCode;
        this.pckgName = pckgName;
        this.impNotes = impNotes;
    }

    public String getPckgCode() {
        return pckgCode;
    }

    public void setPckgCode(String pckgCode) {
        this.pckgCode = pckgCode;
    }

    public String getPckgName() {
        return pckgName;
    }

    public void setPckgName(String pckgName) {
        this.pckgName = pckgName;
    }

    public String getImpNotes() {
        return impNotes;
    }

    public void setImpNotes(String impNotes) {
        this.impNotes = impNotes;
    }
}
