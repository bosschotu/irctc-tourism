package com.spicemoney.tourism.provider;

import com.spicemoney.tourism.dto.responseDto.PackageSearchDto;
import com.spicemoney.tourism.encryption.EncryptionException;
import com.spicemoney.tourism.provider.irctc.responseDto.PackageItineraryResponseDto;
import com.spicemoney.tourism.provider.irctc.responseDto.PackageListResponseDto;
import com.spicemoney.tourism.provider.irctc.responseDto.PackageTermsDetailsResponseDto;

import java.util.List;
import java.util.Optional;

public interface Provider {
    List<PackageSearchDto> getPackageSearchkey(String searchKey, String travelType);
    List<PackageListResponseDto> searchPackage(String source, String destination, String travelType) throws EncryptionException;
    PackageItineraryResponseDto getPackageItinerary(String pckgCode);
    PackageTermsDetailsResponseDto getPackageTermsDetails(String pckgCode);
//    void searchPackage(String source, String travelType);
}
