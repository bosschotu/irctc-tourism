package com.spicemoney.tourism.controller;

import com.spicemoney.tourism.dto.responseDto.TourismResponse;
import com.spicemoney.tourism.dto.responseDto.TourismSuccessResponse;
import com.spicemoney.tourism.provider.Provider;
import com.spicemoney.tourism.provider.irctc.responseDto.PackageItineraryResponseDto;
import com.spicemoney.tourism.provider.irctc.responseDto.PackageTermsDetailsResponseDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RestController
public class TestController {

    @Autowired
    private Provider irctcTourism;

    @PostMapping("/getPackageDetails")
    public ResponseEntity<TourismResponse> getPackageList(HttpServletRequest request) {
        irctcTourism.searchPackage("Delhi", "Mumbai", "Domestic");
        return new ResponseEntity<>(TourismSuccessResponse.success("Done"), HttpStatus.OK);
    }

    @PostMapping("/getPackageIternary")
    public ResponseEntity<?> getPackageListIt(HttpServletRequest request) {
        PackageItineraryResponseDto message = irctcTourism.getPackageItinerary("NDR01W");
        return new ResponseEntity<>(message, HttpStatus.OK);
    }

    @PostMapping("/getPackageTerms")
    public ResponseEntity<?> getPackageListItt(HttpServletRequest request) {
        PackageTermsDetailsResponseDto message = irctcTourism.getPackageTermsDetails("NDR01W");
        return new ResponseEntity<>(message, HttpStatus.OK);
    }
}
