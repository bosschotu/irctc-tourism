package com.spicemoney.tourism.encryption;

import com.spicemoney.tourism.exception.TourismException;

import static com.spicemoney.tourism.exception.ErrorCode.ENCRYPTION_EXCEPTION;

public class EncryptionException extends TourismException {

    public EncryptionException(String message) {
        super(ENCRYPTION_EXCEPTION, message);
    }

    public EncryptionException(String message, Throwable cause) {
        super(ENCRYPTION_EXCEPTION, message, cause);
    }

    public EncryptionException(Throwable cause) {
        super(ENCRYPTION_EXCEPTION, cause);
    }

}
