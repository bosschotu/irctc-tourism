package com.spicemoney.tourism.encryption;

public interface EncryptionService {
    String encrypt(String input) throws EncryptionException;
    String decrypt(String cipherText) throws EncryptionException;
}
