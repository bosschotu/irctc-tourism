package com.spicemoney.tourism.service;

import java.util.Locale;

public interface TourismMessageSource {
    
    public String getMessage(String messageCode);

    public String getMessage(String messageCode, Locale locale);

    public String getMessage(String messageCode, Locale locale, String defaultMessage);
}
