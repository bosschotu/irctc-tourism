package com.spicemoney.tourism.dto.responseDto;

public enum ResponseStatus {

    SUCCESS("Success"),
    FAILURE("Failure");

    private final String status;

    ResponseStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return status;
    }

    public String getStatus() {
        return status;
    }
}
