package com.spicemoney.tourism.dto.responseDto;

public interface TourismResponse {
    ResponseStatus status();
    String message();
}
