package com.spicemoney.tourism.dto.responseDto;

public class PackageSearchDto {
    private String searchKey;
    private String tagType;

    public PackageSearchDto() {
    }

    public PackageSearchDto(String searchKey, String tagType) {
        this.searchKey = searchKey;
        this.tagType = tagType;
    }

    public String getSearchKey() {
        return searchKey;
    }

    public void setSearchKey(String searchKey) {
        this.searchKey = searchKey;
    }

    public String getTagType() {
        return tagType;
    }

    public void setTagType(String tagType) {
        this.tagType = tagType;
    }
}
