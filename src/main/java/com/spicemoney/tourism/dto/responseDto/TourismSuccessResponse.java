package com.spicemoney.tourism.dto.responseDto;

import java.time.ZonedDateTime;
import java.util.Optional;

public class TourismSuccessResponse<T> implements TourismResponse {
    private final ResponseStatus status;
    private final ZonedDateTime timestamp;
    private final String message;
    private final Optional<T> data;

    public TourismSuccessResponse(ResponseStatus status, ZonedDateTime timestamp, String message, Optional<T> data) {
        this.status = status;
        this.timestamp = timestamp;
        this.message = message;
        this.data = data;
    }

    public static <T> TourismSuccessResponse<T> of(ResponseStatus status, ZonedDateTime timestamp, String message, T data) {
        return new TourismSuccessResponse<>(status, timestamp, message, Optional.ofNullable(data));
    }

    public static <T> TourismSuccessResponse<T> of(ResponseStatus status, String message, T data) {
        return new TourismSuccessResponse<>(status, ZonedDateTime.now(), message, Optional.ofNullable(data));
    }

    public static <T> TourismSuccessResponse<T> of(ResponseStatus status, String message) {
        return new TourismSuccessResponse<>(status, ZonedDateTime.now(), message, Optional.empty());
    }

    public static <T> TourismSuccessResponse<T> success(T data) {
        return new TourismSuccessResponse<>(ResponseStatus.SUCCESS, ZonedDateTime.now(), "Success", Optional.ofNullable(data));
    }

    @Override
    public ResponseStatus status() {
        return status;
    }

    @Override
    public String message() {
        return message;
    }

    public ZonedDateTime timestamp() {
        return timestamp;
    }

    public Optional<T> data() {
        return data;
    }
}
