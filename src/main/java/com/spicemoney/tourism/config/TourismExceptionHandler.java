package com.spicemoney.tourism.config;

import com.spicemoney.tourism.exception.ErrorCode;
import com.spicemoney.tourism.exception.TourismException;
import com.spicemoney.tourism.exception.TourismExceptionResponse;
import com.spicemoney.tourism.service.TourismMessageSource;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.util.Optional;

@ControllerAdvice
public class TourismExceptionHandler {

    private static final Logger logger = LoggerFactory.getLogger(TourismExceptionHandler.class);

    @Value("${spice.money.tourism.send.stacktrace.in.response}")
    private boolean sendStacktraceInResponse;

    @Autowired
    private TourismMessageSource messageSource;

    @ExceptionHandler(value = TourismException.class)
    public ResponseEntity<TourismExceptionResponse> handleTourismException(TourismException ex) {
        logger.error(ex.getMessage(), ex);
        ErrorCode errorCode = ex.getErrorCode();
        return new ResponseEntity<>(TourismExceptionResponse.of(errorCode.status(), messageSource.getMessage(errorCode.messageCode()), stackTrace(ex)), errorCode.httpStatus());
    }

    private Optional<String> stackTrace(Throwable ex) {
        if (sendStacktraceInResponse) {
            return Optional.of(ExceptionUtils.getStackTrace(ex));
        } else {
            return Optional.empty();
        }
    }
}
