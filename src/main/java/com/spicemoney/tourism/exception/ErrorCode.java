package com.spicemoney.tourism.exception;

import com.spicemoney.tourism.dto.responseDto.ResponseStatus;
import org.springframework.http.HttpStatus;

public enum ErrorCode {
    INTERNAL_SERVER_ERROR(HttpStatus.INTERNAL_SERVER_ERROR, ResponseStatus.FAILURE, "INTERNAL_SERVER_ERROR_MSG"),
    ENCRYPTION_EXCEPTION(HttpStatus.INTERNAL_SERVER_ERROR, ResponseStatus.FAILURE, "ENCRYPTION_EXCEPTION_MSG");


    private final HttpStatus httpStatus;
    private final ResponseStatus status;
    private final String messageCode;


    ErrorCode(HttpStatus httpStatus, ResponseStatus status, String messageCode) {
        this.httpStatus = httpStatus;
        this.status = status;
        this.messageCode = messageCode;
    }

    public HttpStatus httpStatus() {
        return httpStatus;
    }

    public ResponseStatus status() {
        return status;
    }

    public String messageCode() {
        return messageCode;
    }
}
