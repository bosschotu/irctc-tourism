package com.spicemoney.tourism.exception;

public class TourismException extends RuntimeException {

    private final ErrorCode errorCode;

    public TourismException(ErrorCode errorCode, String message) {
        super(message);
        this.errorCode = errorCode;
    }

    public TourismException(ErrorCode errorCode, String message, Throwable cause) {
        super(message, cause);
        this.errorCode = errorCode;
    }

    public TourismException(ErrorCode errorCode, Throwable cause) {
        super(cause);
        this.errorCode = errorCode;
    }

    public ErrorCode getErrorCode() {
        return errorCode;
    }
}
