package com.spicemoney.tourism.exception;

import com.spicemoney.tourism.dto.responseDto.ResponseStatus;
import com.spicemoney.tourism.dto.responseDto.TourismResponse;

import java.time.ZonedDateTime;
import java.util.Optional;

public class TourismExceptionResponse implements TourismResponse {
    private final ResponseStatus status;
    private final ZonedDateTime timestamp;
    private final String message;
    private final Optional<String> stackTrace;

    public TourismExceptionResponse(ResponseStatus status, ZonedDateTime timestamp, String message, Optional<String> stackTrace) {
        this.status = status;
        this.timestamp = timestamp;
        this.message = message;
        this.stackTrace = stackTrace;
    }

    public static TourismExceptionResponse of(ResponseStatus status, String message, Optional<String> stackTrace) {
        return new TourismExceptionResponse(status, ZonedDateTime.now(), message, stackTrace);
    }

    public static TourismExceptionResponse of(ResponseStatus status, ZonedDateTime timestamp, String message) {
        return new TourismExceptionResponse(status, timestamp, message, Optional.empty());
    }

    public static TourismExceptionResponse of(ResponseStatus status, String message) {
        return new TourismExceptionResponse(status, ZonedDateTime.now(), message, Optional.empty());
    }

    @Override
    public ResponseStatus status() {
        return status;
    }

    public ZonedDateTime timestamp() {
        return timestamp;
    }

    public String message() {
        return message;
    }

    public Optional<String> stackTrace() {
        return stackTrace;
    }
}
