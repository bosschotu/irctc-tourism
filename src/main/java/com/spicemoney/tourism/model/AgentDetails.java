package com.spicemoney.tourism.model;

import javax.persistence.*;

@Entity
@Table(name = "agent_details")
public class AgentDetails {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long clientId;

    private String agentName;
    private String agentMobile;
    private String password;


}


